#Makefile
CC = g++
FLAGS1 = -g -Wpedantic -Wall -std=c++11
FLAGS2 = -pedantic -errors -Werror
DEST = ./bin/
SORC = ./src/


all:  debug clean compile 
	@echo "Finished"


compile:
	$(CC) $(FLAGS1) $(SORC)main.cpp $(SORC)FileOperations.cpp -o $(DEST)FileParser.out


clean:
	@echo "Removing old files"
	rm -f $(DEST)*.out

debug:
	@echo "Compiling"

r:
	@echo "Executing file" 
	./bin/FileParser.out


