#include "FileOperations.h"

using namespace std;

void czytaj_plik_przyklad()
{
    char buff[255];

    FILE* wsk_plik;
    wsk_plik = fopen(PLIK_1, "rt");
    if(wsk_plik == 0)
    {
        cout<<"Blad otwarcia pliku"<<endl;
        exit(0);
    }
    
    fgets(buff, 255, wsk_plik);
    cout<<buff<<endl;

    fclose(wsk_plik);
}